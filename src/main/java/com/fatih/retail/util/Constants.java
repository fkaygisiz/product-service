package com.fatih.retail.util;

public class Constants {

  public static final String ROLE_ADMIN = "ROLE_ADMIN";
  public static final String ROLE_USER = "ROLE_USER";
  private Constants() {
  }
}
