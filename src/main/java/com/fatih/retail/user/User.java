package com.fatih.retail.user;

import com.fatih.retail.config.dao.AuditedEntity;
import com.fatih.retail.role.Role;
import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "user_info")
public class User extends AuditedEntity<UUID> implements UserDetails {

  private static final long serialVersionUID = 1804881349896397186L;

  @NotNull
  @Column(name = "first_name")
  private String firstName;

  @NotNull
  @Column(name = "last_name")
  private String lastName;

  @NotNull
  @Email
  @Column(name = "email_address", unique = true)
  private String email;

  @NotNull
  private String password;

  @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
  @JoinTable(name = "user_roles", joinColumns = {
      @JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)}, inverseJoinColumns = {
      @JoinColumn(name = "role_id", referencedColumnName = "id", insertable = false, updatable = false)})
  private Set<Role> roles;

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return roles;
  }

  @Override
  public String getUsername() {
    return email;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }
}