package com.fatih.retail.user;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserModifyDto extends UserDto {

  @NotBlank
  @Size(min = 2, max = 32, message = "password must be between 2 and 8 characters long")
  private String password;
}
