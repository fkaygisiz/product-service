package com.fatih.retail.user;

import com.fatih.retail.role.RoleDto;
import java.util.Set;
import java.util.UUID;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UserDto {

  protected UUID id;

  @NotBlank
  protected String firstName;

  @NotBlank
  protected String lastName;

  @Email
  @NotNull
  protected String email;

  protected Set<RoleDto> roles;
}
