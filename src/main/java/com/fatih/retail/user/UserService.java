package com.fatih.retail.user;

import com.fatih.retail.role.Role;
import com.fatih.retail.role.RoleRepository;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {

  private UserRepository userRepository;
  private RoleRepository roleRepository;
  private ModelMapper modelMapper;

  @Override
  public UserDetails loadUserByUsername(String username) {
    Optional<User> userByEmail = userRepository.findUserByEmailIgnoreCase(username);
    return userByEmail.orElseThrow(() -> new UsernameNotFoundException("User not found."));
  }

  public UserDto createUserFromDto(UserModifyDto userDto, String roleName) {
    User user = modelMapper.map(userDto, User.class);
    user.setPassword(BCrypt.hashpw(userDto.getPassword(), BCrypt.gensalt(10)));
    User savedUser = this.saveUser(user, roleName);
    return modelMapper.map(savedUser, UserDto.class);
  }

  private User saveUser(User user, String roleName) {
    Role role = roleRepository.findByName(roleName);
    Set<Role> roles = new HashSet<>();
    roles.add(role);
    user.setRoles(roles);
    return userRepository.save(user);
  }

  public Optional<UserDto> findById(UUID id) {
    Optional<User> user = userRepository.findById(id);
    if (user.isPresent()) {
      return Optional.of(modelMapper.map(user.get(), UserDto.class));
    } else {
      return Optional.empty();
    }
  }
}