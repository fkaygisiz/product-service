package com.fatih.retail.user;

import static com.fatih.retail.util.Constants.ROLE_USER;

import java.net.URI;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {

  private UserService userService;

  @PostMapping("/create")
  public ResponseEntity<UserDto> createUser(@Valid @RequestBody UserModifyDto userDto) {
    UserDto savedUser = userService.createUserFromDto(userDto, ROLE_USER);
    URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
        .path("/users/{id}")
        .buildAndExpand(savedUser.getId()).toUri();
    return ResponseEntity.created(location).body(savedUser);
  }

  @GetMapping("/{id}")
  public ResponseEntity<UserDto> getUser(@PathVariable UUID id) {
    Optional<UserDto> savedUser = userService.findById(id);
    if (savedUser.isPresent()) {
      return ResponseEntity.ok(savedUser.get());
    } else {
      return ResponseEntity.noContent().build();
    }

  }
}
