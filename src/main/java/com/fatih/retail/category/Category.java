package com.fatih.retail.category;

import com.fatih.retail.config.dao.AuditedEntity;
import com.fatih.retail.product.Product;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "categories")
public class Category extends AuditedEntity<UUID> {

  @NotNull
  @Column(name = "category_name")
  private String name;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
  private Set<Product> products;

}
