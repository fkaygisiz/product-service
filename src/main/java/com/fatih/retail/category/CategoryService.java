package com.fatih.retail.category;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CategoryService {

  private CategoryRepository categoryRepository;
  private ModelMapper modelMapper;

  public List<CategoryDto> findAll() {
    List<Category> allCategories = categoryRepository.findAll();
    return Arrays.asList(modelMapper.map(allCategories, CategoryDto[].class));
  }

  public Optional<CategoryDto> findById(UUID id) {
    Optional<Category> category = categoryRepository.findById(id);
    if (category.isPresent()) {
      return Optional.of(modelMapper.map(category.get(), CategoryDto.class));
    } else {
      return Optional.empty();
    }
  }

  public Optional<Category> getCategory(UUID id) {
    return categoryRepository.findById(id);
  }
}
