package com.fatih.retail.category;

import com.fatih.retail.product.ProductForCategoryDto;
import java.util.Set;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CategoryDto {

  @NotNull
  private UUID id;

  @NotBlank
  private String name;

  private Set<ProductForCategoryDto> products;

}
