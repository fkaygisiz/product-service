package com.fatih.retail.category;

import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CategoryForProductDto {

  @NotNull
  private UUID id;

  @NotBlank
  private String name;

}
