package com.fatih.retail.category;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/categories")
public class CategoryController {

  private CategoryService categoryService;

  @GetMapping
  public ResponseEntity<List<CategoryDto>> getCategories() {

    List<CategoryDto> allCategories = categoryService.findAll();
    if (CollectionUtils.isEmpty(allCategories)) {
      return ResponseEntity.noContent().build();
    }
    return ResponseEntity.ok(allCategories);
  }

  @GetMapping("/{id}")
  public ResponseEntity<CategoryDto> getCategory(@PathVariable(required = true) UUID id) {
    Optional<CategoryDto> category = categoryService.findById(id);
    if (category.isPresent()) {
      return ResponseEntity.ok(category.get());
    } else {
      return ResponseEntity.noContent().build();
    }
  }

}
