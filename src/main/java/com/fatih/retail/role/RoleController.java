package com.fatih.retail.role;

import static com.fatih.retail.util.Constants.ROLE_ADMIN;

import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/roles")
@AllArgsConstructor
public class RoleController {

  private RoleService roleService;

  @GetMapping
  @Secured(ROLE_ADMIN)
  public ResponseEntity<List<RoleDto>> getAll() {
    List<RoleDto> roles = roleService.findAll();
    if (CollectionUtils.isEmpty(roleService.findAll())) {
      return ResponseEntity.noContent().build();
    } else {
      return ResponseEntity.ok(roles);
    }
  }
}
