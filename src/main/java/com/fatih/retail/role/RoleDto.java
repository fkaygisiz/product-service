package com.fatih.retail.role;

import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class RoleDto {

  @NotNull
  private UUID id;

  @NotNull
  private String name;
}
