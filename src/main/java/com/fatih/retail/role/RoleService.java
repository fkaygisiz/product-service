package com.fatih.retail.role;

import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RoleService {

  private RoleRepository roleRepository;
  private ModelMapper modelMapper;

  public List<RoleDto> findAll() {
    return Arrays.asList(modelMapper.map(roleRepository.findAll(), RoleDto[].class));
  }

}
