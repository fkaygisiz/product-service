package com.fatih.retail.role;

import com.fatih.retail.config.dao.AuditedEntity;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "roles")
public class Role extends AuditedEntity<UUID> implements GrantedAuthority {

  private static final long serialVersionUID = -1247406341738269879L;

  @NotNull
  @Column(name = "role_name")
  private String name;

  @Override
  public String getAuthority() {
    return getName();
  }

}