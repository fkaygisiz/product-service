package com.fatih.retail.config;

import com.fatih.retail.config.validation.ValidationErrorResponse;
import com.fatih.retail.config.validation.Violation;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice
class ErrorHandlingControllerAdvice {

  private static final String ERROR_CODE_PREFIX = "Error code is {}";

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  ValidationErrorResponse onMethodArgumentNotValidException(MethodArgumentNotValidException e) {
    ValidationErrorResponse error = new ValidationErrorResponse();
    for (FieldError fieldError : e.getBindingResult().getFieldErrors()) {
      error.getViolations().add(new Violation(fieldError.getField(), fieldError.getDefaultMessage()));
    }
    return error;
  }

  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public String handle(Exception ex, HttpServletRequest request, HttpServletResponse response) {
    UUID randomUUID = UUID.randomUUID();
    log.error(ERROR_CODE_PREFIX, randomUUID.toString(), ex);
    return "Please check logs. Error code is " + randomUUID.toString();
  }

  @ExceptionHandler(AccessDeniedException.class)
  @ResponseStatus(HttpStatus.FORBIDDEN)
  @ResponseBody
  public String handleAccessDeniedExceptions(Exception ex, HttpServletRequest request, HttpServletResponse response) {
    UUID randomUUID = UUID.randomUUID();
    log.error(ERROR_CODE_PREFIX, randomUUID.toString(), ex);
    return "Please check logs. Error code is " + randomUUID.toString();
  }

  @ExceptionHandler(DataIntegrityViolationException.class)
  @ResponseStatus(HttpStatus.CONFLICT)
  @ResponseBody
  public String handleDataAlreadyExist(Exception ex, HttpServletRequest request, HttpServletResponse response) {
    UUID randomUUID = UUID.randomUUID();
    log.error(ERROR_CODE_PREFIX, randomUUID.toString(), ex);
    return "Data already exists. Please check logs. Error code is " + randomUUID.toString();
  }
}