package com.fatih.retail.product;

import com.fatih.retail.category.Category;
import com.fatih.retail.category.CategoryService;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ProductService {

  private CategoryService categoryService;
  private ProductRepository productRepository;
  private ModelMapper modelMapper;

  public List<ProductDto> findAll() {
    List<Product> allProduct = productRepository.findAll();
    return Arrays.asList(modelMapper.map(allProduct, ProductDto[].class));
  }

  public Optional<ProductDto> findById(UUID id) {
    Optional<Product> product = productRepository.findById(id);
    if (product.isPresent()) {
      return Optional.of(modelMapper.map(product.get(), ProductDto.class));
    }
    return Optional.empty();

  }

  public ProductDto save(ProductDto productDto) {
    Product product = modelMapper.map(productDto, Product.class);
    Product savedProduct = productRepository.save(product);
    return modelMapper.map(savedProduct, ProductDto.class);
  }

  public Optional<ProductDto> updateProduct(ProductDto productDto, UUID id) {
    Optional<Product> product = productRepository.findById(id);
    Optional<Category> category = categoryService.getCategory(productDto.getCategory().getId());
    if (product.isPresent() && category.isPresent()) {
      Product productFromDbUpdated = product.get();
      BeanUtils.copyProperties(productDto, productFromDbUpdated);
      productFromDbUpdated.setCategory(category.get());
      Product savedProduct = productRepository.save(productFromDbUpdated);
      return Optional.of(modelMapper.map(savedProduct, ProductDto.class));
    } else {
      return Optional.empty();
    }
  }

  public boolean deleteProduct(UUID id) {
    Optional<Product> product = productRepository.findById(id);
    if (product.isPresent()) {
      productRepository.deleteById(id);
      return true;
    } else {
      return false;
    }

  }

}
