package com.fatih.retail.product;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/products")
@AllArgsConstructor
public class ProductController {

  private ProductService productService;

  @GetMapping
  public ResponseEntity<List<ProductDto>> getProducts() {
    List<ProductDto> allProducts = productService.findAll();
    if (CollectionUtils.isEmpty(allProducts)) {
      return ResponseEntity.noContent().build();
    }
    return ResponseEntity.ok(allProducts);
  }

  @GetMapping("/{id}")
  public ResponseEntity<ProductDto> getProduct(@PathVariable(required = true) UUID id) {
    Optional<ProductDto> product = productService.findById(id);
    if (product.isPresent()) {
      return ResponseEntity.ok(product.get());
    }
    return ResponseEntity.noContent().build();
  }

  @PostMapping
  public ResponseEntity<ProductDto> addProduct(@Valid @RequestBody ProductDto productDto) {
    ProductDto savedProduct = productService.save(productDto);
    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
        .buildAndExpand(savedProduct.getId()).toUri();

    return ResponseEntity.created(location).body(savedProduct);
  }

  @PutMapping("/{id}")
  public ResponseEntity<ProductDto> updateProduct(@PathVariable UUID id, @Valid @RequestBody ProductDto productDto) {
    Optional<ProductDto> product = productService.updateProduct(productDto, id);
    if (product.isPresent()) {
      return ResponseEntity.ok(product.get());
    } else {
      return ResponseEntity.noContent().build();
    }

  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteProduct(@PathVariable UUID id) {

    if (productService.deleteProduct(id)) {
      return ResponseEntity.ok().build();
    } else {
      return ResponseEntity.noContent().build();
    }

  }

}
