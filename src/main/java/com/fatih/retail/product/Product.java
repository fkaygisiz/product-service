package com.fatih.retail.product;

import com.fatih.retail.category.Category;
import com.fatih.retail.config.dao.AuditedEntity;
import java.math.BigDecimal;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "products")
public class Product extends AuditedEntity<UUID> {

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "category")
  private Category category;

  @Column(name = "product_name", unique = true)
  private String name;

  @Column(name = "price")
  private BigDecimal price;
}
