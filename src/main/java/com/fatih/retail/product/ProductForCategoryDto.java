package com.fatih.retail.product;

import java.math.BigDecimal;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductForCategoryDto {

  private UUID id;

  @NotBlank
  private String name;

  @NotNull
  private BigDecimal price;

}
