package com.fatih.retail.product;

import com.fatih.retail.category.CategoryForProductDto;
import java.math.BigDecimal;
import java.util.UUID;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductDto {

  private UUID id;

  private CategoryForProductDto category;

  @NotBlank
  private String name;

  @NotNull
  @DecimalMin(message = "Price cannot be less than 0.1", value = "0.1")
  private BigDecimal price;

}
