package com.fatih.retail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.fatih.retail")
public class RetailServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(RetailServiceApplication.class, args);
  }

}
