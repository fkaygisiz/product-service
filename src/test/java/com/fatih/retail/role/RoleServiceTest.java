package com.fatih.retail.role;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class RoleServiceTest {


  @Mock
  private RoleRepository roleRepository;

  @Spy
  private ModelMapper modelMapper;

  @InjectMocks
  private RoleService roleService;

  @Test
  public void givenRolesExistInDbWhenRolesAreRequestedThenRoleDtosReturn() {
    List<Role> allRoles = new ArrayList<>();
    allRoles.add(new Role());
    allRoles.add(new Role());
    when(roleRepository.findAll()).thenReturn(allRoles);
    List<RoleDto> allRolesFromDb = roleService.findAll();
    assertEquals(2, allRolesFromDb.size());
  }

}
