package com.fatih.retail.role;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class RoleControllerTest {

  @Mock
  private RoleService roleService;

  @InjectMocks
  private RoleController roleController;

  @Test
  public void givenNoRoleExistsWhenRolesAreRequestedThenReturnResponseEntityNoContent() {
    when(roleService.findAll()).thenReturn(new ArrayList<>());
    ResponseEntity<List<RoleDto>> roles = roleController.getAll();
    assertEquals(HttpStatus.NO_CONTENT, roles.getStatusCode());
  }

  @Test
  public void givenTwoRolesExistWhenRolesAreRequestedThenReturnResponseEntityOk() {
    List<RoleDto> rolesFromService = new ArrayList<>();
    rolesFromService.add(new RoleDto());
    rolesFromService.add(new RoleDto());
    when(roleService.findAll()).thenReturn(rolesFromService);
    ResponseEntity<List<RoleDto>> roles = roleController.getAll();
    assertEquals(HttpStatus.OK, roles.getStatusCode());
    assertEquals(2, Objects.requireNonNull(roles.getBody()).size());
  }
}