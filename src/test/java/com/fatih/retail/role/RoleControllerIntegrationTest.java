package com.fatih.retail.role;

import static org.junit.Assert.assertEquals;

import com.fatih.retail.AbstractIntegrationTest;
import com.fatih.retail.RetailServiceApplication;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException.Forbidden;
import org.springframework.web.client.RestTemplate;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailServiceApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class RoleControllerIntegrationTest extends AbstractIntegrationTest {

  @LocalServerPort
  int port;


  @Autowired
  private TestRestTemplate testRestTemplate;


  //TestRestTemplate doesnt work well with http 403 error
  @Test(expected = Forbidden.class)
  public void givenRolesExistsInDBWhenRolesIsCalledByRegularUserThenGetHttpForbidden() {
    HttpHeaders headers = new HttpHeaders();
    headers.setBasicAuth("reg.user@a.com", "pw");
    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

    HttpEntity<String> request = new HttpEntity<>(headers);
    new RestTemplate().exchange("http://localhost:" + port + "/roles",
        HttpMethod.GET, request, new ParameterizedTypeReference<String>() {
        });
  }

  @Test
  public void givenRolesExistsInDBWhenRolesIsCalledByAdminUserThenGetHttpOk() {
    ResponseEntity<List<RoleDto>> result = testRestTemplate.withBasicAuth("admin@a.com", "pw").exchange("/roles",
        HttpMethod.GET, null, new ParameterizedTypeReference<List<RoleDto>>() {
        });
    assertEquals(HttpStatus.OK, result.getStatusCode());
    assertEquals(2, Objects.requireNonNull(result.getBody()).size());
  }

}
