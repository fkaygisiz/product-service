package com.fatih.retail.product;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@RunWith(SpringJUnit4ClassRunner.class)
public class ProductControllerTest {

  @Mock
  private ProductService productService;

  @InjectMocks
  private ProductController productController;

  @Test
  public void givenNoProductExistsWhenProductsAreRequestedThenReturnResponseEntityNoContent() {
    when(productService.findAll()).thenReturn(new ArrayList<>());
    ResponseEntity<List<ProductDto>> products = productController.getProducts();
    assertEquals(HttpStatus.NO_CONTENT, products.getStatusCode());
  }

  @Test
  public void givenTwoProductsExistWhenProductsAreRequestedThenReturnResponseEntityOk() {
    List<ProductDto> rolesFromService = new ArrayList<>();
    rolesFromService.add(new ProductDto());
    rolesFromService.add(new ProductDto());
    when(productService.findAll()).thenReturn(rolesFromService);
    ResponseEntity<List<ProductDto>> products = productController.getProducts();
    assertEquals(HttpStatus.OK, products.getStatusCode());
    assertEquals(2, Objects.requireNonNull(products.getBody()).size());
  }

  @Test
  public void givenNoProductExistsWhenProductIsRequestedByIdThenReturnResponseEntityNoContent() {
    UUID id = UUID.randomUUID();
    when(productService.findById(id)).thenReturn(Optional.empty());
    ResponseEntity<ProductDto> products = productController.getProduct(id);
    assertEquals(HttpStatus.NO_CONTENT, products.getStatusCode());
  }

  @Test
  public void givenProductExistsWhenProductIsRequestedByIdThenReturnResponseEntityOk() {
    UUID id = UUID.randomUUID();
    when(productService.findById(id)).thenReturn(Optional.of(new ProductDto()));
    ResponseEntity<ProductDto> products = productController.getProduct(id);
    assertEquals(HttpStatus.OK, products.getStatusCode());
  }

  @Test
  public void givenUserModifyDtoWhenCreatePorductIsRequestedThenReturnHttpOk() {
    MockHttpServletRequest request = new MockHttpServletRequest();
    request.setRequestURI("/products");
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    ProductDto productDto = new ProductDto();
    UUID id = UUID.randomUUID();
    productDto.setId(id);
    when(productService.save(any())).thenReturn(productDto);
    ResponseEntity<ProductDto> productDtoResponseEntity = productController.addProduct(productDto);
    assertEquals(HttpStatus.CREATED, productDtoResponseEntity.getStatusCode());
    assertEquals("http://localhost/products/" + id, Objects.requireNonNull(productDtoResponseEntity.getHeaders().get("Location")).get(0));
  }

  @Test
  public void givenProductDoesntExistsInDbWhenUpdateProductIsRequestedThenReturnResponseEntityNoContent() {
    UUID id = UUID.randomUUID();
    when(productService.updateProduct(any(), eq(id))).thenReturn(Optional.empty());
    ResponseEntity<ProductDto> products = productController.updateProduct(id, new ProductDto());
    assertEquals(HttpStatus.NO_CONTENT, products.getStatusCode());
  }

  @Test
  public void givenProductExistsInDbWhenUpdateProductIsRequestedThenReturnResponseEntityOk() {
    UUID id = UUID.randomUUID();
    ProductDto mockProduct = new ProductDto();
    mockProduct.setId(id);
    mockProduct.setName("new name");
    when(productService.updateProduct(any(), eq(id))).thenReturn(Optional.of(mockProduct));
    ResponseEntity<ProductDto> product = productController.updateProduct(id, new ProductDto());
    assertEquals(HttpStatus.OK, product.getStatusCode());
    assertEquals("new name", Objects.requireNonNull(product.getBody()).getName());
  }

  @Test
  public void givenProductDoesntExistInDbWhenDeleteProductIsRequestedThenReturnResponseEntityNoContent() {
    UUID id = UUID.randomUUID();
    when(productService.deleteProduct(id)).thenReturn(false);
    ResponseEntity<Void> responseEntity = productController.deleteProduct(id);
    assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
  }

  @Test
  public void givenProductExistsInDbWhenDeleteProductIsRequestedThenReturnResponseEntityOk() {
    UUID id = UUID.randomUUID();
    when(productService.deleteProduct(id)).thenReturn(true);
    ResponseEntity<Void> responseEntity = productController.deleteProduct(id);
    assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
  }
}