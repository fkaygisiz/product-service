package com.fatih.retail.product;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import com.fatih.retail.AbstractIntegrationTest;
import com.fatih.retail.RetailServiceApplication;
import com.fatih.retail.category.Category;
import com.fatih.retail.category.CategoryForProductDto;
import com.fatih.retail.category.CategoryRepository;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailServiceApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProductControllerIntegrationTest extends AbstractIntegrationTest {

  @Autowired
  private TestRestTemplate restTemplate;

  @Autowired
  private ProductRepository productRepository;

  @Autowired
  private CategoryRepository categoryRepository;

  @Test
  public void givenCorrectUserNameAndPasswordWhenProductsIsCalledThenGetHttpOk() {
    Product product = new Product();
    product.setName("integration test product");
    product.setPrice(BigDecimal.valueOf(12212.23));
    Product savedProduct = productRepository.save(product);
    ResponseEntity<List<ProductDto>> result = restTemplate.withBasicAuth("reg.user@a.com", "pw").exchange("/products",
        HttpMethod.GET, null, new ParameterizedTypeReference<List<ProductDto>>() {
        });
    assertEquals(HttpStatus.OK, result.getStatusCode());
    productRepository.delete(savedProduct);
  }

  @Test
  public void givenWrongUserNameAndPasswordWhenProductsIsCalledThenGetHttpUnauthorized() {
    ResponseEntity<List<ProductDto>> result = restTemplate.withBasicAuth("reg.user@a.com", "pwwrong").exchange("/products",
        HttpMethod.GET, null, new ParameterizedTypeReference<List<ProductDto>>() {
        });
    assertEquals(HttpStatus.UNAUTHORIZED, result.getStatusCode());
  }

  @Test
  public void givenNoProductExistInDbWhenProductsIsCalledThenGetHttpNoContent() {
    productRepository.deleteAll();
    ResponseEntity<List<ProductDto>> result = restTemplate.withBasicAuth("reg.user@a.com", "pw").exchange("/products",
        HttpMethod.GET, null, new ParameterizedTypeReference<List<ProductDto>>() {
        });
    assertEquals(HttpStatus.NO_CONTENT, result.getStatusCode());
  }

  @Test
  public void givenUuidExistsInDbWhenProductsIsCalledWithThisUuidThenGetHttpOk() {

    Product product = new Product();
    product.setName("integration test product");
    product.setPrice(BigDecimal.valueOf(12212.23));
    Product savedProduct = productRepository.save(product);
    ResponseEntity<ProductDto> result = restTemplate.withBasicAuth("reg.user@a.com", "pw").exchange(
        "/products/" + savedProduct.getId(), HttpMethod.GET, null,
        new ParameterizedTypeReference<ProductDto>() {
        });
    assertEquals(HttpStatus.OK, result.getStatusCode());
    assertEquals(savedProduct.getId(), Objects.requireNonNull(result.getBody()).getId());
    productRepository.delete(savedProduct);
  }

  @Test
  public void givenProductNameDoesntExistInDbWhenSaveProductsIsCalledThenGetHttpOk() {

    ProductDto product = new ProductDto();
    product.setName("integration test product2");
    product.setPrice(BigDecimal.valueOf(12212.23));

    HttpEntity<ProductDto> requestEntity = new HttpEntity<>(product);
    ResponseEntity<ProductDto> result = restTemplate.withBasicAuth("reg.user@a.com", "pw").exchange("/products/",
        HttpMethod.POST, requestEntity, new ParameterizedTypeReference<ProductDto>() {
        });
    assertEquals(HttpStatus.CREATED, result.getStatusCode());
    productRepository.deleteById(Objects.requireNonNull(result.getBody()).getId());
  }

  @Test
  public void givenProductNameExistsInDbWhenSaveProductsIsCalledThenGetHttpConflict() {

    Product product = new Product();
    product.setName("integration test product3");
    product.setPrice(BigDecimal.valueOf(12212.23));
    Product savedProduct = productRepository.save(product);

    ProductDto productDto = new ProductDto();
    productDto.setName("integration test product3");
    productDto.setPrice(BigDecimal.valueOf(12212.23));

    HttpEntity<ProductDto> requestEntity = new HttpEntity<>(productDto);
    ResponseEntity<String> result = restTemplate.withBasicAuth("reg.user@a.com", "pw").exchange("/products/",
        HttpMethod.POST, requestEntity, new ParameterizedTypeReference<String>() {
        });
    assertEquals(HttpStatus.CONFLICT, result.getStatusCode());
    productRepository.delete(savedProduct);
  }

  @Test
  public void givenProductCategoryDoesntExistInDbWhenSaveProductsIsCalledThenGetHttpConflict() {

    ProductDto productDto = new ProductDto();
    productDto.setName("integration test product4");
    productDto.setPrice(BigDecimal.valueOf(12212.23));
    CategoryForProductDto category = new CategoryForProductDto();
    category.setId(UUID.randomUUID());
    category.setName("random category");
    productDto.setCategory(category);

    HttpEntity<ProductDto> requestEntity = new HttpEntity<>(productDto);
    ResponseEntity<String> result = restTemplate.withBasicAuth("reg.user@a.com", "pw").exchange("/products/",
        HttpMethod.POST, requestEntity, new ParameterizedTypeReference<String>() {
        });
    assertEquals(HttpStatus.CONFLICT, result.getStatusCode());
  }

  @Test
  public void givenProductExistsInDbWhenUpdateProductsIsCalledThenGetHttpOkAndNameIsUpdatedInDb() {

    Category category = categoryRepository.findAll().get(0);
    Product product = new Product();
    product.setName("integration test product5");
    product.setPrice(BigDecimal.valueOf(12212.23));
    product.setCategory(category);
    Product savedProduct = productRepository.save(product);

    ProductDto productDto = new ProductDto();
    productDto.setId(savedProduct.getId());
    productDto.setName("integration test product6");
    productDto.setPrice(BigDecimal.valueOf(12212.23));

    CategoryForProductDto categoryDto = new CategoryForProductDto();
    categoryDto.setId(category.getId());
    categoryDto.setName(category.getName());
    productDto.setCategory(categoryDto);

    HttpEntity<ProductDto> requestEntity = new HttpEntity<>(productDto);
    ResponseEntity<ProductDto> result = restTemplate.withBasicAuth("reg.user@a.com", "pw").exchange(
        "/products/" + savedProduct.getId(), HttpMethod.PUT, requestEntity,
        new ParameterizedTypeReference<ProductDto>() {
        });
    assertEquals(HttpStatus.OK, result.getStatusCode());
    Optional<Product> updatedProduct = productRepository.findById(savedProduct.getId());
    assertEquals("integration test product6", updatedProduct.get().getName());
    productRepository.delete(savedProduct);

  }

  @Test
  public void givenProductExistsInDbWhenDeleteProductsIsCalledThenGetHttpOkAndProductIsDeletedFromDb() {
    Product product = new Product();
    product.setName("integration test product5");
    product.setPrice(BigDecimal.valueOf(12212.23));
    Product savedProduct = productRepository.save(product);
    ResponseEntity<Void> result = restTemplate.withBasicAuth("reg.user@a.com", "pw").exchange(
        "/products/" + savedProduct.getId(), HttpMethod.DELETE, null, new ParameterizedTypeReference<Void>() {
        });
    assertEquals(HttpStatus.OK, result.getStatusCode());
    Optional<Product> deletedProduct = productRepository.findById(savedProduct.getId());
    assertFalse(deletedProduct.isPresent());
    productRepository.delete(savedProduct);
  }

  @Test
  public void givenProductDoesntExistsInDbWhenDeleteProductsIsCalledThenGetHttpNoContent() {

    ResponseEntity<Void> result = restTemplate.withBasicAuth("reg.user@a.com", "pw").exchange(
        "/products/" + UUID.randomUUID(), HttpMethod.DELETE, null, new ParameterizedTypeReference<Void>() {
        });
    assertEquals(HttpStatus.NO_CONTENT, result.getStatusCode());
  }
}
