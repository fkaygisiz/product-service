package com.fatih.retail.product;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import com.fatih.retail.category.Category;
import com.fatih.retail.category.CategoryForProductDto;
import com.fatih.retail.category.CategoryService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class ProductServiceTest {

  @Mock
  private ProductRepository productRepository;

  @Spy
  private ModelMapper modelMapper;

  @Mock
  private CategoryService categoryService;

  @InjectMocks
  private ProductService productService;

  @Test
  public void given1PoductExistsInDbWhenAllProductsAreRequestedThenGet1Product() {
    List<Product> allProductsFromDb = new ArrayList<>();
    allProductsFromDb.add(new Product());
    when(productRepository.findAll()).thenReturn(allProductsFromDb);
    List<ProductDto> allProducts = productService.findAll();
    assertEquals(1, allProducts.size());
  }

  @Test
  public void given1PoductIdExistsInDbWhenProductIsRequestedByIdThenGet1Product() {
    Product product = new Product();
    UUID id = UUID.randomUUID();
    product.setId(id);
    Optional<Product> optionalProduct = Optional.of(product);
    when(productRepository.findById(id)).thenReturn(optionalProduct);
    Optional<ProductDto> result = productService.findById(id);
    assertTrue(result.isPresent());
  }

  @Test
  public void givenNoPoductIdExistsInDbWhenProductIsRequestedByIdThenGetEmptyOption() {
    UUID id = UUID.randomUUID();
    Optional<Product> optionalProduct = Optional.empty();
    when(productRepository.findById(id)).thenReturn(optionalProduct);
    Optional<ProductDto> result = productService.findById(id);
    assertFalse(result.isPresent());
  }

  @Test
  public void givenProductDtoWhenProductIsRequestedToSaveThenResultHasAnId() {
    ProductDto productDto = new ProductDto();
    productDto.setName("mock product");
    Product mockDbProduct = new Product();
    UUID id = UUID.randomUUID();
    mockDbProduct.setId(id);
    when(productRepository.save(any(Product.class))).thenReturn(mockDbProduct);
    ProductDto result = productService.save(productDto);
    assertEquals(id, result.getId());
  }

  @Test
  public void givenCategoryDoesntExistInDbWhenProductIsRequestedToUpdateThenEmptyOptionalIsGot() {
    ProductDto productDto = new ProductDto();
    UUID productId = UUID.randomUUID();
    productDto.setName("mock product");
    productDto.setId(productId);
    CategoryForProductDto category = new CategoryForProductDto();
    UUID categoryId = UUID.randomUUID();
    category.setId(categoryId);
    productDto.setCategory(category);

    Product mockDbProduct = new Product();
    mockDbProduct.setId(productId);
    when(productRepository.findById(productId)).thenReturn(Optional.of(mockDbProduct));
    when(categoryService.getCategory(categoryId)).thenReturn(Optional.empty());

    Optional<ProductDto> result = productService.updateProduct(productDto, productId);
    assertFalse(result.isPresent());
  }

  @Test
  public void givenCategoryAndProductExistInDbWhenProductIsRequestedToUpdateThenUpdatedProductIsGot() {
    ProductDto productDto = new ProductDto();
    UUID productId = UUID.randomUUID();
    productDto.setName("new name");
    productDto.setId(productId);

    CategoryForProductDto category = new CategoryForProductDto();
    UUID categoryId = UUID.randomUUID();
    category.setId(categoryId);
    productDto.setCategory(category);

    Product mockDbProduct = new Product();
    mockDbProduct.setId(productId);
    mockDbProduct.setName("old name");
    when(productRepository.findById(productId)).thenReturn(Optional.of(mockDbProduct));
    Optional<Category> categoryFromDb = Optional.of(modelMapper.map(category, Category.class));
    when(categoryService.getCategory(categoryId)).thenReturn(categoryFromDb);
    when(productRepository.save(any(Product.class))).thenAnswer(i -> i.getArguments()[0]);
    Optional<ProductDto> result = productService.updateProduct(productDto, productId);
    assertTrue(result.isPresent());
    assertEquals("new name", result.get().getName());
  }


  @Test
  public void givenProductDoesntExistInDbWhenDeleteIsRequestedThenGetFalse() {
    UUID productId = UUID.randomUUID();
    when(productRepository.findById(productId)).thenReturn(Optional.empty());
    boolean productIsDeleted = productService.deleteProduct(productId);
    assertFalse(productIsDeleted);
  }

  @Test
  public void givenProductExistSInDbWhenDeleteIsRequestedThenGetTrue() {
    UUID productId = UUID.randomUUID();
    Optional<Product> product = Optional.of(new Product());
    when(productRepository.findById(productId)).thenReturn(product);
    doNothing().when(productRepository).deleteById(productId);
    boolean productIsDeleted = productService.deleteProduct(productId);
    assertTrue(productIsDeleted);
  }
}
