package com.fatih.retail.user;

import static com.fatih.retail.util.Constants.ROLE_USER;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerTest {

  private final ModelMapper modelMapper = new ModelMapper();
  @Mock
  private UserService userService;
  @InjectMocks
  private UserController userController;

  @Test
  public void givenUserModifyDtoWhenCreateUserIsRequestedThenReturnHttpOk() {
    MockHttpServletRequest request = new MockHttpServletRequest();
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

    UserModifyDto userModifyDto = new UserModifyDto();
    userModifyDto.setPassword("pw");
    userModifyDto.setEmail("fat.kay@com");
    userModifyDto.setFirstName("fat");
    userModifyDto.setLastName("kay");
    UserDto userDtoFromService = modelMapper.map(userModifyDto, UserDto.class);
    UUID id = UUID.randomUUID();
    userDtoFromService.setId(id);
    when(userService.createUserFromDto(userModifyDto, ROLE_USER)).thenReturn(userDtoFromService);
    ResponseEntity<UserDto> user = userController.createUser(userModifyDto);
    assertEquals(HttpStatus.CREATED, user.getStatusCode());
    assertEquals("http://localhost/users/" + id, Objects.requireNonNull(user.getHeaders().get("Location")).get(0));
  }

  @Test
  public void givenUserExistsInDbWhenUserIsCalledByIdThenReturnHttpOk() {
    Optional<UserDto> userFromService = Optional.of(new UserDto());
    when(userService.findById(any())).thenReturn(userFromService);
    ResponseEntity<UserDto> user = userController.getUser(UUID.randomUUID());
    assertEquals(HttpStatus.OK, user.getStatusCode());
  }

  @Test
  public void givenUserDoesntExistInDbWhenUserIsCalledByIdThenReturnHttpNoContent() {
    Optional<UserDto> userFromService = Optional.empty();
    when(userService.findById(any())).thenReturn(userFromService);
    ResponseEntity<UserDto> user = userController.getUser(UUID.randomUUID());
    assertEquals(HttpStatus.NO_CONTENT, user.getStatusCode());
  }

}