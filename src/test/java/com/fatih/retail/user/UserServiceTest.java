package com.fatih.retail.user;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fatih.retail.role.RoleRepository;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {

  @Mock
  private UserRepository userRepository;
  @Mock
  private RoleRepository roleRepository;

  @Spy
  private ModelMapper modelMapper;

  @InjectMocks
  private UserService userService;

  @Test(expected = UsernameNotFoundException.class)
  public void givenEmailDoesntExistInDbWhenLoadByUserNameIsRequestedThenReturnUserNotFoundException() {
    when(userRepository.findUserByEmailIgnoreCase(any(String.class))).thenReturn(Optional.empty());
    userService.loadUserByUsername("fat.kay@a.com");
  }

  @Test
  public void givenEmailExistsInDbWhenLoadByUserNameIsRequestedThenReturnUserDetails() {
    User user = new User();
    user.setEmail("fat.kay@a.com");

    when(userRepository.findUserByEmailIgnoreCase(any(String.class))).thenReturn(Optional.of(user));
    UserDetails userDetails = userService.loadUserByUsername("fat.kay@a.com");
    assertEquals("fat.kay@a.com", userDetails.getUsername());
  }
}
