package com.fatih.retail.user;

import static org.junit.Assert.assertEquals;

import com.fatih.retail.AbstractIntegrationTest;
import com.fatih.retail.RetailServiceApplication;
import com.fatih.retail.util.Constants;
import java.util.Objects;
import java.util.UUID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailServiceApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class UserControllerIntegrationTest extends AbstractIntegrationTest {

  @Autowired
  private TestRestTemplate restTemplate;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Test
  public void givenUserDoesNotExistInDbWhenCreateUserIsCalledThenGetHttpCreated() {
    UserModifyDto newUser = new UserModifyDto();
    newUser.setEmail("new.user@a.com");
    newUser.setFirstName("fatih");
    newUser.setLastName("Kayg");
    newUser.setPassword("pw");

    HttpEntity<UserModifyDto> requestEntity = new HttpEntity<>(newUser);
    ResponseEntity<UserDto> result = restTemplate.exchange("/users/create", HttpMethod.POST, requestEntity,
        new ParameterizedTypeReference<UserDto>() {
        });
    assertEquals(HttpStatus.CREATED, result.getStatusCode());
    assertEquals(Constants.ROLE_USER, Objects.requireNonNull(result.getBody()).getRoles().stream().findAny().get().getName());

    userRepository.deleteById(result.getBody().getId());
  }

  @Test
  public void givenUserExistsInDbWhenCreateUserIsCalledThenGetHttpConflict() {
    UserModifyDto newUser = new UserModifyDto();
    newUser.setEmail("new.user@a.com");
    newUser.setFirstName("fatih");
    newUser.setLastName("Kayg");
    newUser.setPassword("pw");

    User userEntity = modelMapper.map(newUser, User.class);
    userEntity.setPassword(BCrypt.hashpw(newUser.getPassword(), BCrypt.gensalt(10)));
    User savedUser = userRepository.save(userEntity);
    HttpEntity<UserModifyDto> requestEntity = new HttpEntity<>(newUser);
    ResponseEntity<String> result = restTemplate.exchange("/users/create", HttpMethod.POST, requestEntity,
        new ParameterizedTypeReference<String>() {
        });
    assertEquals(HttpStatus.CONFLICT, result.getStatusCode());
    userRepository.deleteById(savedUser.getId());
  }

  @Test
  public void givenPasswordIsOneLetterLongWhenCreateUserIsCalledThenGetHttpBadRequest() {
    UserModifyDto newUser = new UserModifyDto();
    newUser.setEmail("new.user@a.com");
    newUser.setFirstName("fatih");
    newUser.setLastName("Kayg");
    newUser.setPassword("p");

    HttpEntity<UserModifyDto> requestEntity = new HttpEntity<>(newUser);
    ResponseEntity<String> result = restTemplate.exchange("/users/create", HttpMethod.POST, requestEntity,
        new ParameterizedTypeReference<String>() {
        });
    assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());

  }

  @Test
  public void givenUserExistsInDbWhenGetUserIsCalledThenGetHttpOk() {
    UserModifyDto newUser = new UserModifyDto();
    newUser.setEmail("new.user@a.com");
    newUser.setFirstName("fatih");
    newUser.setLastName("Kayg");
    newUser.setPassword("pw");

    User userEntity = modelMapper.map(newUser, User.class);
    userEntity.setPassword(BCrypt.hashpw(newUser.getPassword(), BCrypt.gensalt(10)));
    User savedUser = userRepository.save(userEntity);

    ResponseEntity<UserDto> result = restTemplate.withBasicAuth("new.user@a.com", "pw").exchange(
        "/users/" + savedUser.getId(), HttpMethod.GET, null, new ParameterizedTypeReference<UserDto>() {
        });
    assertEquals(HttpStatus.OK, result.getStatusCode());
    assertEquals(savedUser.getEmail(), Objects.requireNonNull(result.getBody()).getEmail());
    assertEquals(savedUser.getFirstName(), result.getBody().getFirstName());
    assertEquals(savedUser.getLastName(), result.getBody().getLastName());

    userRepository.deleteById(result.getBody().getId());
  }

  @Test
  public void givenUserDoesNotExistInDbWhenGetUserIsCalledThenGetHttpNoContent() {
    ResponseEntity<UserDto> result = restTemplate.withBasicAuth("admin@a.com", "pw").exchange(
        "/users/" + UUID.randomUUID(), HttpMethod.GET, null, new ParameterizedTypeReference<UserDto>() {
        });
    assertEquals(HttpStatus.NO_CONTENT, result.getStatusCode());
  }

  @Test
  public void givenPasswordHasNineLettersWhenCreateUserIsCalledThenGetHttpBadRequest() {
    UserModifyDto newUser = new UserModifyDto();
    newUser.setEmail("new.user@a.com");
    newUser.setFirstName("fatih");
    newUser.setLastName("Kayg");
    newUser.setPassword("p");

    HttpEntity<UserModifyDto> requestEntity = new HttpEntity<>(newUser);
    ResponseEntity<String> result = restTemplate.exchange("/users/create", HttpMethod.POST, requestEntity,
        new ParameterizedTypeReference<String>() {
        });
    assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
  }
}
