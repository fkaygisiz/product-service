package com.fatih.retail.category;

import static org.junit.Assert.assertEquals;

import com.fatih.retail.AbstractIntegrationTest;
import com.fatih.retail.RetailServiceApplication;
import java.util.List;
import java.util.Objects;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RetailServiceApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class CategoryControllerIntegrationTest extends AbstractIntegrationTest {

  @Autowired
  private TestRestTemplate restTemplate;

  @Autowired
  private CategoryRepository categoryRepository;

  @Test
  public void givenCorrectUserNameAndPasswordWhenCategoriesIsCalledThenGetHttpOk() {
    ResponseEntity<List<CategoryDto>> result = restTemplate.withBasicAuth("reg.user@a.com", "pw").exchange("/categories",
        HttpMethod.GET, null, new ParameterizedTypeReference<List<CategoryDto>>() {
        });
    assertEquals(HttpStatus.OK, result.getStatusCode());
  }

  @Test
  public void givenNoCategoryExistsWhenCategoriesIsCalledThenGetHttpNoContent() {
    List<Category> allCategories = categoryRepository.findAll();
    categoryRepository.deleteAll();
    ResponseEntity<List<CategoryDto>> result = restTemplate.withBasicAuth("reg.user@a.com", "pw").exchange("/categories",
        HttpMethod.GET, null, new ParameterizedTypeReference<List<CategoryDto>>() {
        });
    assertEquals(HttpStatus.NO_CONTENT, result.getStatusCode());
    categoryRepository.saveAll(allCategories);
  }

  @Test
  public void given3CategoriesExistInDbWhenCategoriesIsCalledThenGet3Categories() {
    ResponseEntity<List<CategoryDto>> result = restTemplate.withBasicAuth("reg.user@a.com", "pw").exchange("/categories",
        HttpMethod.GET, null, new ParameterizedTypeReference<List<CategoryDto>>() {
        });
    assertEquals(HttpStatus.OK, result.getStatusCode());
    assertEquals(3, Objects.requireNonNull(result.getBody()).size());
  }

  @Test
  public void givenCategoryExistsWhenCategoryByIdIsCalledThenGetHttpOk() {
    Category existingCategory = categoryRepository.findAll().get(0);
    ResponseEntity<CategoryDto> result = restTemplate.withBasicAuth("reg.user@a.com", "pw").exchange(
        "/categories/" + existingCategory.getId(), HttpMethod.GET, null,
        new ParameterizedTypeReference<CategoryDto>() {
        });
    assertEquals(HttpStatus.OK, result.getStatusCode());
    assertEquals(existingCategory.getName(), Objects.requireNonNull(result.getBody()).getName());
  }

}
