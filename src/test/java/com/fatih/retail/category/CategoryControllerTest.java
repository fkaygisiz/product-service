package com.fatih.retail.category;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class CategoryControllerTest {

  @Mock
  private CategoryService categoryService;

  @InjectMocks
  private CategoryController categoryController;

  @Test
  public void givenNoCategoryExistWhenCategoriesAreRequestedThenReturnResponseEntityNoContent() {
    when(categoryService.findAll()).thenReturn(new ArrayList<>());
    ResponseEntity<List<CategoryDto>> categories = categoryController.getCategories();
    assertEquals(HttpStatus.NO_CONTENT, categories.getStatusCode());
  }

  @Test
  public void givenOneCategoryExistsWhenCategoriesAreRequestedThenReturnResponseEntityOk() {
    List<CategoryDto> categoriesFromDb = new ArrayList<>();
    categoriesFromDb.add(new CategoryDto());
    when(categoryService.findAll()).thenReturn(categoriesFromDb);
    ResponseEntity<List<CategoryDto>> categories = categoryController.getCategories();
    assertEquals(HttpStatus.OK, categories.getStatusCode());
    assertEquals(1, Objects.requireNonNull(categories.getBody()).size());
  }

  @Test
  public void givenNoCategoryExistWhenCategoryByIdIsRequestedThenReturnResponseEntityNoContent() {
    when(categoryService.findById(any())).thenReturn(Optional.empty());
    ResponseEntity<CategoryDto> category = categoryController.getCategory(UUID.randomUUID());
    assertEquals(HttpStatus.NO_CONTENT, category.getStatusCode());
  }

  @Test
  public void givenOneCategoryExistsWhenCategoryByIdIsRequestedThenReturnResponseEntityOk() {
    UUID id = UUID.randomUUID();
    CategoryDto categoryFromService = new CategoryDto();
    categoryFromService.setId(id);
    categoryFromService.setName("first category");
    when(categoryService.findById(id)).thenReturn(Optional.of(categoryFromService));
    ResponseEntity<CategoryDto> category = categoryController.getCategory(id);
    assertEquals(HttpStatus.OK, category.getStatusCode());
    assertEquals(id, Objects.requireNonNull(category.getBody()).getId());
    assertEquals("first category", category.getBody().getName());

  }
}