package com.fatih.retail.category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class CategoryServiceTest {

  @Mock
  private CategoryRepository categoryRepository;

  @Spy
  private ModelMapper modelMapper;

  @InjectMocks
  private CategoryService categoryService;


  @Test
  public void given1CategoryExistsInDbWhenAllCategoriesAreRequestedThenGet1Category() {
    List<Category> allCategoriesFromDb = new ArrayList<>();
    allCategoriesFromDb.add(new Category());
    when(categoryRepository.findAll()).thenReturn(allCategoriesFromDb);
    List<CategoryDto> allCategories = categoryService.findAll();
    assertEquals(1, allCategories.size());
  }

  @Test
  public void given1CategoryExistsWithKnownIdDbWhenCategoryIsRequestedWithThatIdThenGetCorrectCategory() {
    Category mockCategory = new Category();
    mockCategory.setName("mockCategory");
    UUID id = UUID.randomUUID();
    mockCategory.setId(id);
    Optional<Category> optionalCategory = Optional.of(mockCategory);
    when(categoryRepository.findById(id)).thenReturn(optionalCategory);
    Optional<CategoryDto> result = categoryService.findById(id);
    assertEquals(id, result.get().getId());
    assertEquals("mockCategory", result.get().getName());
  }

  @Test
  public void given1CategoryDoesntExistsWithKnownIdDbWhenCategoryIsRequestedWithThatIdThenGetEmptyOptional() {
    UUID id = UUID.randomUUID();
    Optional<Category> optionalCategory = Optional.empty();
    when(categoryRepository.findById(id)).thenReturn(optionalCategory);
    Optional<CategoryDto> result = categoryService.findById(id);
    assertFalse(result.isPresent());
  }


}
