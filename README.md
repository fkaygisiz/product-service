---
This is a sample product manipulation project for a retail company.
---


Application uses postgresql as DB (RDS service in DB) and already deployed to AWS beanstalk.

Integration tests are using test containers and in the integration tests postresql is also used. So application needs Docker environment for real and test environments.

Sample docker command to run to create a DB:

```bash
docker run --name docker-postgres -e POSTGRES_PASSWORD=postgres1 -d -p 5432:5432 -v /data/docker/postgres:/var/lib/postgresql/data postgres:13-alpine
```

Postman collection is in documents folder.

3 categories and 2 users with different roles are directly saved into db at the initialization by Flyway.

Usernames are 'admin@a.com' and 'reg.user@a.com', passwords is 'pw' for both of them.

---

Categories are not editable. However, products can be added to or removed from a category.

--- 